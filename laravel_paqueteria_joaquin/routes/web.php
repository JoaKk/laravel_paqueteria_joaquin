<?php

use App\Http\Controllers\InicioController;
use App\Http\Controllers\PaqueteController;
use App\Http\Controllers\TransportistaController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [InicioController::class, 'inicio']);
Route::get('transportistas', [TransportistaController::class, 'index'])->name('transportistas.index');
Route::get('transportistas/{transportista}',[TransportistaController::class,'show'])->name('transportistas.show');
Route::get('transportistas/{transportista}/entregar',[TransportistaController::class,'entregar'])->name('transportistas.entregar');
Route::get('transportistas/{transportista}/noentregado',[TransportistaController::class,'noentregado'])->name('transportistas.noentregado');
Route::get('paquetes/crear',[PaqueteController::class,'create'])->name('paquetes.create');
Route::post('paquetes',[PaqueteController::class,'store'])->name('paquetes.store');

