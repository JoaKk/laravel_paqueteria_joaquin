<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Transportista extends Model
{
    protected $table = 'transportistas';
    use HasFactory;

    public function empresas(){
        return $this->belongsToMany(Empresa::class);
    }

    public function paquetes()
    {
        return $this->hasMany(Paquete::class);
    }

    public function getRouteKeyName()
    {
        return 'slug';
    }   

    public function getPermisoConducirAno()
    {
        $fechaFormateada = Carbon::parse($this->fechaPermisoConducir);
        return $fechaFormateada->diffInYears(Carbon::now());
    }
}
