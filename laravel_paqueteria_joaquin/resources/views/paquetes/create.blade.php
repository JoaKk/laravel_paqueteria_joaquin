@extends('layouts.master')
@section('titulo')
    Transportistas
@endsection
@section('contenido')
    <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-4 ">
            <div class="card">
                <div class="card-header">Añadir nuevo paquete</div>
                <div class="card-body">
                    <form action="{{route('paquetes.store')}}"  method="POST" enctype="multipart/form-data">
                        @csrf
                        <label for="direccion_entrega">Dirección de entrega</label>
                        <input type="text" name="direccion_entrega">
                        <hr>
                        <label for="transportista">Transportista</label>
                        <select name="transportista">
                            @foreach ($transportistas as $transportista)
                                <option value="{{ $transportista->id }}">{{ $transportista->nombre }}
                                    {{ $transportista->apellidos }}</option>
                            @endforeach
                        </select>
                        <hr>
                        <label for="imagen">Imagen </label>
                        <input type="file" name="imagen" id="imagen" />
                        <button type="submit" class="btn btn-primary">Añadir paquete</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('css')
    <link rel="stylesheet" href="{{ url('/assets/bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ url('/assets/bootstrap/js/bootstrap.min.js') }}">
    <link rel="stylesheet" href="{{ url('/assets/bootstrap/css/estilo.css') }}">
@endsection
