<?php

namespace App\Http\Controllers;

use App\Models\Transportista;
use Illuminate\Http\Request;

class InicioController extends Controller
{
    public function inicio()
    {
        return redirect()->action([TransportistaController::class,'index']);
    }
}
