<?php

namespace Database\Factories;

use App\Models\Paquete;
use App\Models\Transportista;
use Illuminate\Database\Eloquent\Factories\Factory;

class PaqueteFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Paquete::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'direccion_entrega' => $this->faker->text(),
            'entregado' => $this->faker->randomElement([0,1]),
            'transportista_id' => Transportista::all()->random()->id,
            'imagen' => 'paquete_por_defecto.jpg'
        ];
    }
}
