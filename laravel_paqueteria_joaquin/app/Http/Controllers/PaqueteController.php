<?php

namespace App\Http\Controllers;

use App\Models\Paquete;
use App\Models\Transportista;
use Illuminate\Http\Request;

class PaqueteController extends Controller
{
    public function create(){
        $transportistas = Transportista::all();
        return view('paquetes.create',compact('transportistas'));
    }

    public function store(Request $request)
    {
        $paquete = new Paquete();
        $paquete->direccion_entrega = $request->direccion_entrega;
        $path = $request->imagen->store('','paquetes');
        $paquete->imagen = $path;
        $paquete->transportista_id = $request->transportista;
        $paquete->save();
        $transportista = Transportista::all()->where('id',$request->transportista)->first();
        return view('transportistas.show',compact('transportista'));
    }
}
