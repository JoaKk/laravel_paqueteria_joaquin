@extends('layouts.master')
@section('titulo')
    Transportistas
@endsection
@section('contenido')
    <div class="row">
        @foreach ($transportistas as $clave => $transportista)
            <div class="col-xs-12 col-sm-6 col-md-4 ">
                <img src="{{asset("/assets/imagenes/transportistas/".$transportista->imagen) }}" alt="">
                <h4><a href="{{route('transportistas.show',$transportista)}}">{{ $transportista->nombre }}</a></h4>
                <small>{{$transportista->paquetes->count()}} paquetes pendientes de entregar</small>
            </div>
        @endforeach
    </div>
@endsection
@section('css')
    <link rel="stylesheet" href="{{ url('/assets/bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ url('/assets/bootstrap/js/bootstrap.min.js') }}">
    <link rel="stylesheet" href="{{ url('/assets/bootstrap/css/estilo.css') }}">
@endsection
