@extends('layouts.master')
@section('titulo')
    Transportistas
@endsection
@section('contenido')
    <div class="row">      
        @php
            if(isset($mensaje)){
               
                echo "<div style='background-color: greenyellow;height:50px;''>$mensaje";
            }
        @endphp
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4 ">              
        <img src="{{asset("/assets/imagenes/transportistas/".$transportista->imagen) }}" alt="">
        <h3>{{$transportista->nombre}} {{$transportista->apellidos}}</h3>
        <h4>Años de permiso de circulación: {{$transportista->getPermisoConducirAno()}}</h4>
        <h4>Empresas:</h4>
        <ul>
            @foreach ($transportista->empresas as $empresa)
                <li>{{$empresa->nombre}}</li>
            @endforeach
        </ul>
        <h4>Paquetes</h4>
        @foreach ($transportista->paquetes as $paquete)
            <small>Paquete {{$paquete->id}} - {{$paquete->direccion_entrega}}: @if ($paquete->entregado) <strong>Entregado</strong> @else <strong> Pendiente de entrega </strong> @endif  </small>
            <br>
        @endforeach        
        <a class="btn btn-primary" role="button" href="{{route('transportistas.entregar',$transportista)}}">Entregar todo</a>
        <a class="btn btn-info" role="button" href="{{route('transportistas.noentregado',$transportista)}}">Marcar todo como no entregado</a>
        </div>
    </div>
@endsection

