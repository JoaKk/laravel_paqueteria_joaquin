<?php

namespace App\Http\Controllers;

use App\Models\Transportista;
use Illuminate\Http\Request;

class TransportistaController extends Controller
{

    public function index()
    {
        $transportistas = Transportista::all();
        return view("transportistas.index",compact('transportistas'));
    }

    public function show(Transportista $transportista){
        return view('transportistas.show',compact('transportista'));
    }

    public function entregar(Transportista $transportista){
        $paquetes = $transportista->paquetes;
        foreach ($paquetes as $paquete) {
            $paquete->entregado = 1;
            $paquete->save();
        }
        return view('transportistas.show',compact('transportista'))->with("mensaje", "Se han entregado todos los paquetes");
    }

    public function noentregado(Transportista $transportista){
        $paquetes = $transportista->paquetes;
        foreach ($paquetes as $paquete) {
            $paquete->entregado = 0;
            $paquete->save();
        } 
        return view('transportistas.show',compact('transportista'))->with("mensaje", "Se han marcado todos los paquetes como no entregados");
    }

}
